<p align="center">
  <img src="https://cdn-images-1.medium.com/max/280/1*ZtdxmoygPBpZFETAE4AoFw@2x.png" width="120" alt="Nest Logo" />
  <img src="https://miro.medium.com/max/400/1*nP2C50GK4_-ly_R_mq3juQ.png" width="120" alt="Maicol" />
</p>

<h1 align="center">
  ONLINE STORE FOR FIRST NEED ITEMS - COVID 19
</h1>

## Description

<p ALIGN="justify">
The next project to be carried out will attack the main problem of crowding of people in the different markets, causing the increase in covid-19 infections. This project is intended to be implemented in the different regions of Peru. The main objective of this project is to program the distribution of basic necessity products by zones assigned by the supplier. This solution would benefit both the supplier and the client since the supplier will be able to establish an intermediate price between the wholesale and the retail apart from the social benefit in which each client will be prevented from accessing the markets. Initially, the sale of rice and sugar sacks will begin, a web application that will be progressively updated.
</p>

This project has using the following features:

- **NodeJs** 12.x+
- **QuasarJs** 1.9.x
- **GraphQl**
- **JWT**

## Installation

### Development

1. Clone Repository

   ```bash
   $ git clone https://gitlab.com/utp-pregrado/curso-integrador-ii-marzo-2020/grupo-1-utp/grupo-1.git
   ```

2. Install the dependencies

   ```bash
   yarn
   ```

3. Start the app in development mode (hot-code reloading, error reporting, etc.)

   ```bash
   quasar dev
   ```

### Production

1. Clone Repository

   ```bash
   $ git clone https://gitlab.com/utp-pregrado/curso-integrador-ii-marzo-2020/grupo-1-utp/grupo-1.git
   ```

2. Build Dockerfile

   ```bash
   $ docker build -t "frontend-osffni/g1" .
   ```

3. Start project

   ```bash
   $ docker run -itd --name frontend-osffni -p 4000:8080 frontend-osffni/g1
   ```

# Authors

#### [Michael C Rodrigo Apaza ](https://www.facebook.com/MaicolCRodrigo)

#### [Angela Flores Lizarraga ]()

#### [Valeria Ruiz Suclupe ]()

#### [Saúl Kana Nuñoncca ]()

#### [Hilda Quispe Mullisaca]()
