import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [

  {
    path: '',
    name: 'Products',
    component: () => import('pages/product.vue')
  },
  {
    path: 'publicacion/:id',
    name: 'Products_id',
    component: () => import('pages/publish/_id.vue')
  }

]

export default routes
