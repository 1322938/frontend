import { RouteConfig } from 'vue-router'

import pri_ from './private'
import pub_ from './public'
import mid_ from './middle'

const routes: RouteConfig[] = [
  {
    path: '/in',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      ...pri_
      // ...mid_
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      ...mid_
    ],
    meta: {
      nestedAuth: true
    }
  },
  {
    path: '/',
    component: () => import('layouts/PublicLayout.vue'),
    children: [
      ...pub_
    ],
    meta: {
      requiresAuth: false
    }
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
