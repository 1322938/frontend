import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/perfil',
    name: 'Perfil',
    component: () => import('pages/Index.vue')
  },
  {
    path: '/map',
    name: 'Map',
    component: () => import('pages/Index.vue')
  },
  {
    path: '/vender',
    name: 'Sale',
    component: () => import('pages/sale.vue')
  },
  {
    path: '/zonas',
    name: 'Area',
    component: () => import('pages/area.vue')
  },
  {
    path: '/carrito',
    name: 'Car',
    component: () => import('pages/car.vue')
  },
  {
    path: '/social',
    name: 'Social',
    component: () => import('pages/social.vue')
  },
  {
    path: '/balance',
    name: 'Balance',
    component: () => import('pages/balance.vue')
  },
  {
    path: '/ordenes',
    name: 'Orders',
    component: () => import('pages/order.vue')
  },
  {
    path: '/ventas',
    name: 'Sales',
    component: () => import('pages/sales.vue')
  },
  {
    path: '/productos',
    name: 'MeProducts',
    component: () => import('pages/meproduct.vue')
  },
  {
    path: '/compras',
    name: 'Buys',
    component: () => import('pages/buys.vue')
  },
  {
    path: '/compras/:id',
    name: 'Buys_id',
    component: () => import('pages/buy/_id.vue')
  },
  {
    path: '/proveedores',
    name: 'Providers',
    component: () => import('pages/providers.vue')
  },
  {
    path: '/ayuda',
    name: 'Helps',
    component: () => import('pages/report.vue')
  },
  {
    path: '/ayuda/:id/:slug',
    name: 'HelpsID',
    component: () => import('pages/report/_id.vue')
  }
]

export default routes
