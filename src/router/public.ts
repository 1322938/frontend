import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/login',
    name: 'SignIn',
    component: () => import('pages/signin.vue')
  },
  {
    path: '/registro',
    name: 'SignUp',
    component: () => import('pages/signup.vue')
  },
  {
    path: '/validacion',
    name: 'Validation',
    component: () => import('pages/validation.vue')
  },
  {
    path: '/auth/:token',
    name: 'Verified',
    component: () => import('pages/auth/_id.vue')
  }
]

export default routes
