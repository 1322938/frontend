import Vue from 'vue'
import { VueReCaptcha } from 'vue-recaptcha-v3'
import * as dotenv from 'dotenv'

dotenv.config()
// For more options see below
Vue.use(VueReCaptcha, {
  siteKey: process.env.RECAPTCHA_KEY,
  loaderOptions: {
    autoHideBadge: true
  }
})
