/* eslint-disable @typescript-eslint/ban-ts-ignore */
import { boot } from 'quasar/wrappers'

import { from, split } from 'apollo-link'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { getMainDefinition } from 'apollo-utilities'
import { InMemoryCache } from 'apollo-cache-inmemory'

import { StorageState } from '@core/modules/auth/storage.state'
import { session, area, shoppingcar, sale, product, person, chatusers, message, order, buy } from '@core/cache'

import { provide } from '@vue/composition-api'
import { DefaultApolloClient } from '@vue/apollo-composable'
import { notifyUtils } from '@core/modules/utils'

import typeDefs from '@core/graphql/typedefs'
import resolvers from '@core/graphql/resolvers'

import * as dotenv from 'dotenv'
import router from '@/router'

dotenv.config()

const storageState = new StorageState()

// eslint-disable-next-line @typescript-eslint/no-unsafe-call
const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = storageState.getToken()
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const errorLink = onError(({ response }) => {
  try {
    // prevents Unauthorized notification on root path

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (!(response.errors[0].message === 'Unauthorized' && router.history.current.path === '/')) {
      const messageError = response.errors[0].extensions.exception.response.message

      if (messageError) {
        notifyUtils.error(messageError)
      }
    }
  } catch (error) { }
})

const httpLink = new HttpLink({
  uri: process.env.API_GRAPHQL_URL
  // uri: 'http://0.0.0.0:4000/graphql',
  // uri: 'https://api.yarems.net/graphql',
  // headers: getHeaders()
})

const wsLink = new WebSocketLink({
  uri: process.env.API_GRAPHQL_URL_WS,
  options: {
    reconnect: true
  }
})

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  },
  wsLink,
  from([
    errorLink, authLink, httpLink /* httpLink */
  ])
  // httpLink
)

const cache = new InMemoryCache({
  addTypename: false
})

export const apolloClient = new ApolloClient({
  link,
  cache,
  resolvers,
  typeDefs
})

cache.writeData({
  data: {
    session,
    area,
    areas: [area],
    buys: [buy],
    orders: [order],
    shoppingcar,
    sales: [sale],
    sale,
    products: [product],
    persons: [person],
    friends: [person],
    startChatPerson: chatusers,
    messages: [message]
  }
})

export default boot(({ app }) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  app.setup = () => {
    provide(DefaultApolloClient, apolloClient)
  }
})
