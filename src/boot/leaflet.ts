
import { Icon } from 'leaflet'
import 'leaflet/dist/leaflet.css'
import 'leaflet-draw/dist/leaflet.draw.css'
import 'leaflet-toolbar/dist/leaflet.toolbar.css'
import 'leaflet-toolbar/dist/leaflet.toolbar.js'

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
delete Icon.Default.prototype._getIconUrl

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
})

const L = {
  DRAW: null,
  map: null,
  tempLayers: null,
  projectLayers: null
}

export { L }
