import gql from 'graphql-tag'

export const CLIENT_PERSONS = gql`
query {
    persons @client {
        person
        documentNumber
        photo
        fullname
        mobile
        email
        username
        status
    }
}
`

export const CLIENT_FRIENDS = gql`
query {
    friends @client {
        person
        documentNumber
        photo
        fullname
        mobile
        email
        username
        status
    }
}
`
