import gql from 'graphql-tag'

export const CLIENT_CAR = gql`
  query {
    shoppingcar @client {
      igv
      commission
      total
      product {
        quantity
        subtotal
        unitPrice
        location {
          id
          latitud
          longitud
          status
        }
        sale {
          id
          stock
          weight
          area{
            id
            name
            latitud
            longitud
            radio
          }
          product {
            id
            name
            category {
              id
              name
            }
            kind {
              id
              name
            }
          }
        }
      }
    }
  }
`
