import gql from 'graphql-tag'

export const CLIENT_NEW_CHAT_PERSON = gql`
query {
    startChatPerson @client {
        channel
        person
        documentNumber
        photo
        fullname
        mobile
        email
        username
        status
    }
}
`

export const CLIENT_MESSAGES = gql`
query {
    messages @client {
        id
        created
        channel
        from
        to
        message
    }
}
`
