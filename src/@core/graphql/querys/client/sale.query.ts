import gql from 'graphql-tag'

export const CLIENT_SALES = gql`
{
    sales @client {
        id
        person {
            id
            name
            user{
                username
            }
            lastnameOne
            lastnameTwo
        }
        product {
            id
            name
            category {
                id
                name
            }
            kind {
                id
                name
            }
        }
        unitPrice
        stock
        weight
        area {
            id
            name
            latitud
            longitud
            radio
            district {
                id
                name
                province {
                    id
                    name
                    region {
                        id
                        name
                    }
                }
            }
        }
    }
}
`
export const CLIENT_ONE_SALE = gql`
{
    sale @client {
        id
        person {
            id
            name
            user{
                username
            }
            lastnameOne
            lastnameTwo
        }
        product {
            id
            name
            category {
                id
                name
            }
            kind {
                id
                name
            }
        }
        unitPrice
        stock
        weight
        area {
            id
            name
            latitud
            longitud
            radio
            district {
                id
                name
                province {
                    id
                    name
                    region {
                        id
                        name
                    }
                }
            }
        }
    }
}
`
