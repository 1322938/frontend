import gql from 'graphql-tag'

export const CLIENT_ALL_BUYS = gql`
query{
    buys @client{
        id
        payment{
        id
        igv
        commission
        total
        status
        }
        observation
        status
        created
    }
}
`
