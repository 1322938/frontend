import gql from 'graphql-tag'

export const CLIENT_SESSION_QUERY = gql`
  {
    session @client {
      isLogged
      user {
        username
        userKind {
          id
          name
        }
        person {
          id
          documentNumber
          name
          photo
          lastnameOne
          lastnameTwo
        }
        verified
        status
        lastSesion
      }
    }
  }
`
