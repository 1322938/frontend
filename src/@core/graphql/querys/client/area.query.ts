import gql from 'graphql-tag'

export const CLIENT_AREA = gql`
query {
    area @client {
        id
        latitud
        longitud
        radio
        user {
            username
            person{
                id
                name
                lastnameOne
                lastnameTwo
            }
            verified
            status
            lastSesion
        }
        district{
            id
            name
            province{
                id
                name
                region{
                    id
                    name
                }
            }
        }
        observation
        status
    }
}
`

export const CLIENT_AREAS = gql`
query {
    areas @client {
        id
        latitud
        longitud
        radio
        user {
            username
            person{
                id
                name
                lastnameOne
                lastnameTwo
            }
        }
        district{
            id
            name
            province{
                id
                name
                region{
                    id
                    name
                }
            }
        }
        observation
        status
    }
}
`
