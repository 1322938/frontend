import { CLIENT_AREA, CLIENT_AREAS } from './area.query'
import { CLIENT_FRIENDS, CLIENT_PERSONS } from './friend.query'
import { CLIENT_NEW_CHAT_PERSON, CLIENT_MESSAGES } from './chat.query'
import { CLIENT_ALL_ORDERS } from './order.query'
import { CLIENT_ALL_BUYS } from './buy.query'
import { CLIENT_ONE_SALE } from './sale.query'

export {
  CLIENT_AREA,
  CLIENT_FRIENDS,
  CLIENT_PERSONS,
  CLIENT_NEW_CHAT_PERSON,
  CLIENT_MESSAGES,
  CLIENT_AREAS,
  CLIENT_ALL_ORDERS,
  CLIENT_ALL_BUYS,
  CLIENT_ONE_SALE
}
