import gql from 'graphql-tag'

export const CLIENT_PRODUCTS = gql`
{
    products @client {
        id
        name
        category {
            id
            name
        }
        kind {
            id
            name
        } 
    }
}
`
