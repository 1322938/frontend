import gql from 'graphql-tag'

export const GET_REGIONS = gql`
  query allRegions {
    allRegions {
      id
      name
    }
  }
`

export const GET_PROVINCES_REGION = gql`
  query allProvincesRegion($region: ID!) {
    allProvincesRegion(region: $region) {
      id
      name
      region {
        id
        name
      }
    }
  }
`

export const GET_DISTRICTS_PROVINCE = gql`
  query allDistritsProvince($province: ID!) {
    allDistritsProvince(province: $province) {
      id
      name
      province {
        name
        region {
          name
        }
      }
    }
  }
`
