import gql from 'graphql-tag'

export const ALL_BUYS_PERSON = gql`
query allBuysPerson($person: ID!){
  allBuysPerson(
    person: $person
  ){
    id
    payment{
      id
      igv
      commission
      total
      status
    }
    observation
    status
    created
  }
}
`
