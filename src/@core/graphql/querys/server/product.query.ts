import gql from 'graphql-tag'

export const ALL_PRODUCTS = gql`
  query {
    allProducts {
      id
      name
      kind {
        id
        name
      }
      category {
        id
        name
      }
    }
  }
`
export const ALL_PRODUCT_CATEGORYS = gql`
  query {
    allProductCategorys {
      id
      name
      description
      status
    }
  }
`
export const ALL_PRODUCT_KINDS = gql`
  query {
    allProductKinds {
      id
      name
      description
      status
    }
  }
`
