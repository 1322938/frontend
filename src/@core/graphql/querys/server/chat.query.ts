import gql from 'graphql-tag'

export const GET_CHENNEL_USERS = gql`
query getChannelUser($from: String!, $to: String!){
  getChannelUser(
    data: {
      from: $from
      to: $to
    }
  ){
    id
    userCreate
    users
  }
}
`

export const ALL_CHATS_CHANNEL = gql`
query allChatsChannel($channel: String!){
  allChatsChannel(
    channel: $channel
  ){
    id
    created
    channel
    from
    to
    message
  }
}
`
