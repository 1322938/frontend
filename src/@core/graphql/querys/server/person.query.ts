import gql from 'graphql-tag'

export const ALL_PERSONS = gql`
query{
  allPersons{
    id
    user{
      username
      lastSesion
    }
    documentNumber
    name
    lastnameOne
    lastnameTwo
    gender
    phone
    mobile
    email
    photo
  }
}
`
