import gql from 'graphql-tag'

export const ALL_REPORTS = gql`
query{
  allReports{
    id
    user
    type
    title
    description
    responses
    created
    image
  }
}
`

export const ONE_REPORT_ID = gql`
query oneReportID($id: ID!){
    oneReportID(
        id: $id
    ){
    id
    user
    type
    title
    description
    responses
    created
    image
  }
}
`

export const ALL_REPORTS_USER = gql`
query allReportsUser($user: String!){
    allReportsUser(
        user: $user
    ){
    id
    user
    type
    title
    description
    responses
    created
    image
  }
}
`
