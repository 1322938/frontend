import gql from 'graphql-tag'

export const ALL_FRIENDS = gql`
query getFriends($me: String!){
  getFriends(
    me:$me
  ){
    me
    friends
  }
}
`
