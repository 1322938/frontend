import { ME_QUERY } from '@core/graphql/querys/server/me.query'
import {
  GET_REGIONS,
  GET_PROVINCES_REGION,
  GET_DISTRICTS_PROVINCE
} from '@core/graphql/querys/server/ubigeo.query'
import {
  ALL_PRODUCTS,
  ALL_PRODUCT_CATEGORYS,
  ALL_PRODUCT_KINDS
} from './product.query'

import { ALL_AREAS } from './area.query'
import { ALL_SALES, ONE_SALE, ALL_SALES_PERSON } from './sale.query'

import { ALL_ORDERS_BUY, ALL_ORDERS_PERSON, ALL_ORDERS, ALL_ORDERS_PERSON_BUY } from './order.query'
import { ALL_BUYS_PERSON } from './buy.query'

import { ALL_PERSONS } from './person.query'

import { GET_CHENNEL_USERS, ALL_CHATS_CHANNEL } from './chat.query'

import { ALL_REPORTS, ALL_REPORTS_USER, ONE_REPORT_ID } from './report.query'

export {
  ME_QUERY,
  GET_DISTRICTS_PROVINCE,
  GET_PROVINCES_REGION,
  GET_REGIONS,
  ALL_PRODUCTS,
  ALL_PRODUCT_CATEGORYS,
  ALL_PRODUCT_KINDS,
  ALL_AREAS,
  ALL_SALES,
  ALL_ORDERS_BUY,
  ALL_ORDERS_PERSON,
  ALL_BUYS_PERSON,
  ALL_PERSONS,
  GET_CHENNEL_USERS,
  ALL_CHATS_CHANNEL,
  ALL_ORDERS,
  ONE_SALE,
  ALL_REPORTS,
  ALL_REPORTS_USER,
  ONE_REPORT_ID,
  ALL_SALES_PERSON,
  ALL_ORDERS_PERSON_BUY
}
