import gql from 'graphql-tag'

export const ME_QUERY = gql`
  query me {
    me {
      user{
       username
      userKind {
        id
        name
      }
      person {
        id
        documentNumber
        name
        photo
        lastnameOne
        lastnameTwo
      }
      verified
      status
      lastSesion
    }
    token
    }
  }
`
