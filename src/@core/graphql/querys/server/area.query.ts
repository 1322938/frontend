import gql from 'graphql-tag'

export const ALL_AREAS = gql`
query{
  allAreas{
    id
    latitud
    longitud
    radio
    district{
      id
      name
      province{
        id
        name
        region{
          id
          name
        }
      }
    }
    observation
    status
    user{
      username
      person{
        id
        name
        lastnameOne
        lastnameTwo
      }
    }
  }
}
`
