import gql from 'graphql-tag'

export const CREATE_SALE_MUTATION = gql`
mutation createSale(
    $person: ID!
    $product: ID!
    $unitPrice: Float!
    $stock: Int!
    $area: ID!
    $weight: Float!
    $observation: String
){
  createSale(
    data: {
      person: $person
      product: $product
      unitPrice: $unitPrice
      stock: $stock
      area: $area
      weight: $weight
      observation: $observation
    }
  ){
    id
    person{
      id
      name
      user{
        username
      }
      lastnameOne
      lastnameTwo
    }
    product{
      id
      name
      category{
        id
        name
      }
      kind{
        id
        name
      }
    }
    unitPrice
    stock
    weight
    area{
      id
      name
      latitud
      longitud
      radio
      district{
        id
        name
        province{
          id
          name
          region{
            id
            name
          }
        }
      }
    }
  }
}
`

export const UPDATE_SALE_MUTATION = gql`
mutation updateSale(
    $id: ID!
    $person: ID
    $product: ID
    $unitPrice: Float
    $stock: Int
    $area: ID
    $weight: Float
    $observation: String
){
  updateSale(
    id: $id
    data: {
      person: $person
      product: $product
      unitPrice: $unitPrice
      stock: $stock
      area: $area
      weight: $weight
      observation: $observation
    }
  ){
    id
    product{
      id
      name
    }
    unitPrice
    stock
    area{
      id
      latitud
      longitud
      radio
      district{
        id
        name
        province{
          id
          name
          region{
            id
            name
          }
        }
      }
    }
  }
}
`

export const DELETE_SALE_MUTATION = gql`
mutation deleteSale($id:ID!){
  deleteSale(
    id: $id
  ){
    id
    product{
      id
      name
    }
    unitPrice
    stock
    area{
      id
      latitud
      longitud
      radio
      district{
        id
        name
        province{
          id
          name
          region{
            id
            name
          }
        }
      }
    }
  }
}
`
