import gql from 'graphql-tag'

export const SEND_MESSAGE_CHAT_MUTATION = gql`
mutation createChat($channel: String!, $from: String!, $to: String!, $message: String!){
  createChat(
    data: {
      channel: $channel
      from: $from
      to: $to
      message: $message
    }
  ){
    id
    created
    channel
    from
    to
    message
  }
}
`
