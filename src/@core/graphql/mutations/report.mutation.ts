import gql from 'graphql-tag'

export const CREATE_REPORT_MUTATION = gql`
mutation createReport($user: String!, $type: ReportEnum!, $title: String!, $description: String!){
  createReport(
    data: {
      user: $user
      type: $type
      title: $title
      description: $description
    }
  ){
    id
    user
    type
    title
    description
    responses
    created
    image
  }
}
`
export const UPDATE_REPORT_MUTATION = gql`
mutation updateReport($id:ID!,$user: String!, $type: ReportEnum!, $title: String!, $description: String!){
    updateReport(
    id: $id
    data: {
      user: $user
      type: $type
      title: $title
      description: $description
    }
  ){
    id
    user
    type
    title
    description
    responses
    created
    image
  }
}
`

export const DELETE_REPORT_MUTATION = gql`
mutation deleteReport($id: ID!){
  deleteReport(
    id: $id
  ){
    ok
  }
}
`

export const ADD_RESPONSE_RESPORT_MUTATION = gql`
mutation addResposeReport($user: String!,$response: String!, $id: ID!){
  addResposeReport(
    user: $user
    response: $response
    id: $id
  ){
    response
    user
  }
}
`
