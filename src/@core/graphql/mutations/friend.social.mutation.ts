import gql from 'graphql-tag'

export const ADD_FRIENDS_MUTATION = gql`
mutation addFriends($me: String!, $friend: String!){
  addFriends(
    data: {
      me: $me
      friend: $friend
    }
  ){
    me
    friends
  }
}
`

export const DELETE_FRIENDS_MUTATION = gql`
mutation deleteFriends($me: String!, $friend: String!){
  deleteFriends(
    data: {
      me: $me
      friend: $friend
    }
  ){
    me
    friends
  }
}
`
