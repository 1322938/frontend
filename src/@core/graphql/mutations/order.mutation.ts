import gql from 'graphql-tag'

export const CHANGE_STATUS_ORDERS_SALE_MUTATION = gql`
mutation changeStatusOrdersSales($status: ProcessEnum!, $sale: ID!){
  changeStatusOrdersSales(
    status: $status
    sale: $sale
  ){
    ok
  }
}
`
