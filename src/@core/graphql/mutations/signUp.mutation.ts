import gql from 'graphql-tag'

export const SIGN_UP_MUTATION = gql`
  mutation signUp(
    $username: String!
    $password: String!
    $passwordConfirmation: String!
    $documentNumber: String!
    $name: String!
    $lastnameOne: String!
    $lastnameTwo: String!
    $gender: GenderEnum!
    $phone: String
    $mobile: String!
    $email: String!
    $district: ID
    $address: String!
    $birthdate: DateTime!
    $response: String!
  ) {
    signUp(
      data: {
        username: $username
        password: $password
        passwordConfirmation: $passwordConfirmation
        documentNumber: $documentNumber
        name: $name
        lastnameOne: $lastnameOne
        lastnameTwo: $lastnameTwo
        gender: $gender
        phone: $phone
        mobile: $mobile
        email: $email
        district: $district
        address: $address
        birthdate: $birthdate
        response: $response
      }
    ) {
      user {
        username
        userKind {
          id
          name
        }
        person {
          id
          documentNumber
          name
          photo
          lastnameOne
          lastnameTwo
          email
        }
        verified
        status
        lastSesion
      }
      token
    }
  }
`
