import gql from 'graphql-tag'

export const CREATE_BUY_MUTATION = gql`
mutation ($payment: ID!, $person: ID!){
  createBuy(
    data: {
      payment: $payment
      person: $person
    }
  ){
    id
    payment{
      id
      igv
      commission
      total
    }
    person{
      id
      documentNumber
      name
      lastnameOne
      lastnameTwo
      email
    }
    observation
    status
  }
}
`
