import gql from 'graphql-tag'

export const CREATE_AREA_MUTATION = gql`
mutation createArea(
    $name: AreaEnum!,
    $latitud: Float!,
    $longitud: Float!, 
    $radio: Float,
    $user: ID!
    $district: ID!,
    $observation: String
    ){
    createArea(
        data:{
            name: $name
            latitud: $latitud
            longitud: $longitud
            radio: $radio
            user: $user
            district: $district
            observation: $observation
        }
    ){
        id
        name
        latitud
        longitud
        radio
        user {
            username
            person{
                id
                name
                lastnameOne
                lastnameTwo
            }
            verified
            status
            lastSesion
        }
        district{
            id
            name
            province{
                id
                name
                region{
                    id
                    name
                }
            }
        }
        observation
        status
    }
}
`
