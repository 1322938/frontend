import gql from 'graphql-tag'

export const CULQI_MUTATION = gql`
mutation (
    $amount: Int!,
    $currency_code: String!,
    $email: String!,
    $source_id: String!
    ){
    culqiPay(
        data: {
            amount: $amount
            currency_code: $currency_code
            email: $email
            source_id: $source_id
        }
    ){
        payment
    }
}
`

export const CREATE_PAYMENT_MUTATION = gql`
mutation ($igv: Float!, $commission: Float!, $total: Float!){
  createPayment(
    data:{
      igv: $igv
      commission: $commission
      total: $total
    }
  ){
    id
    igv
    commission
    total
    status
  }
}
`
/*
export const PAYMENT_CAR_MAPPING_MUTATION = gql`
mutation ($car: JSON!, $culqi: JSON!){
  paymentCarMapping(
    data:{
      car: $car
      culqi: $culqi
    }
  ){
    buy{
      id
      payment{
        id
        igv
        commission
        total
      }
      person{
        id
        documentNumber
        name
        lastnameOne
        lastnameTwo
      }
      orders{
        id
        locate{
          id
          latitud
          longitud
        }
        quantity
        price
        process
        sale{
          id
          product{
            id
            name
            description
            category{
              id
              name
            }
            kind{
              id
              name
            }
          }
          unitPrice
          stock
          weight
          area{
            id
            name
            latitud
            longitud
            radio
            district{
              id
              name
              province{
                id
                name
                region{
                  id
                  name
                }
              }
            }
          }
        }
      }
    }
    culqi
  }
}
`
*/
export const PAYMENT_CAR_MAPPING_MUTATION = gql`
mutation ($car: JSON!, $culqi: JSON!){
  paymentCarMapping(
    data:{
      car: $car
      culqi: $culqi
    }
  ){
    ok
  }
}
`
