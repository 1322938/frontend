import gql from 'graphql-tag'

export const SET_SESSION_MUTATION = gql`
  mutation ($session: Session) {
    setSession(session: $session) @client {
      session {
        user {
            username
            userKind {
                id
                name
            }
            person {
              id
              documentNumber
              name
              photo
              lastnameOne
              lastnameTwo
            }
            verified
            status
            lastSesion
        }
        isLogged
      }
    }
  }
`
