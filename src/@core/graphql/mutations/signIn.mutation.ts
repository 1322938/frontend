import gql from 'graphql-tag'

export const SIGN_IN_MUTATION = gql`
  mutation signIn($username: String!, $password: String!, $response: String!) {
    signIn(data: { username: $username, password: $password, response: $response }) {
      user {
        username
        userKind {
          id
          name
        }
        person {
          id
          documentNumber
          name
          photo
          lastnameOne
          lastnameTwo
          email
        }
        verified
        status
        lastSesion
      }
      token
    }
  }
`
