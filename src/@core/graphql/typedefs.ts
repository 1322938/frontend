import gql from 'graphql-tag'

export default gql`
type UserKind {
  id: ID!
  name: String!
}
type Person {
  id: ID!
  documentNumber: String!
  name: String!
  lastnameOne: String!
  lastnameTwo: String!
  photo: String
}
type User {
  username: ID!
  userKind: UserKind
  verified: Boolean!
  status: Boolean!
  lastSesion: String
  person: Person
}
type Session {
    user: User
    isLogged: Boolean
}
type Mutation {
    setSession(session: Session): Boolean
}
`
