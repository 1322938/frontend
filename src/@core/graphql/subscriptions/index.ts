import { CHAT_SUBSCRIPTION } from './chat.subscription'
import { SALES_STOCK_SUBCRIPTION, SALE_SUBSCRIPTION } from './sale.subscription'
import { BUY_SUBSCRIPTION } from './buy.subscription'
import { ORDER_SUBSCRIPTION } from './order.subscription'
import { AREA_SUBSCRIPTION } from './area.subscription'
import { PERSON_SUBSCRIPTION } from './person.subscription'

export {
  CHAT_SUBSCRIPTION,
  SALES_STOCK_SUBCRIPTION,
  ORDER_SUBSCRIPTION,
  BUY_SUBSCRIPTION,
  SALE_SUBSCRIPTION,
  AREA_SUBSCRIPTION,
  PERSON_SUBSCRIPTION
}
