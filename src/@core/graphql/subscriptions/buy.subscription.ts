import gql from 'graphql-tag'

export const BUY_SUBSCRIPTION = gql`
subscription buyPersonSubs($personChannel: String!){
  buyPersonSubs(
    personChannel: $personChannel
  ){
    action
    buy{
        id
        payment{
        id
        igv
        commission
        total
        status
        }
        observation
        status
        created
    }
  }
}
`
