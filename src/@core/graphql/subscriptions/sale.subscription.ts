import gql from 'graphql-tag'

export const SALES_STOCK_SUBCRIPTION = gql`
subscription{
  salesStockSubs{
    id
    stock
  }
}
`

export const SALE_SUBSCRIPTION = gql`
subscription{
  saleSubs{
    action
    sale{
      id
        person {
            id
            name
            user{
                username
            }
            lastnameOne
            lastnameTwo
        }
        product {
            id
            name
            category {
                id
                name
            }
            kind {
                id
                name
            }
        }
        unitPrice
        stock
        weight
        area {
            id
            name
            latitud
            longitud
            radio
            district {
                id
                name
                province {
                    id
                    name
                    region {
                        id
                        name
                    }
                }
            }
        }
    }
  }
}
`
