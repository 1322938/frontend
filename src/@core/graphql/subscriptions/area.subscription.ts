import gql from 'graphql-tag'

export const AREA_SUBSCRIPTION = gql`
subscription{
  areaSubs{
    action
    area{
      id,
      name
      latitud
      longitud
      radio
      user {
          username
          person{
              id
              name
              lastnameOne
              lastnameTwo
          }
          verified
          status
          lastSesion
      }
      district{
          id
          name
          province{
              id
              name
              region{
                  id
                  name
              }
          }
      }
      observation
      status
    }
  }
}
`
