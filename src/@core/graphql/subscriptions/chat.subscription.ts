import gql from 'graphql-tag'

export const CHAT_SUBSCRIPTION = gql`
subscription chatSubs($channel: String!){
  chatSubs(
    channel: $channel
  ){
    id
    created
    channel
    from
    to
    message
  }
}
`
