import gql from 'graphql-tag'

export const PERSON_SUBSCRIPTION = gql`
subscription{
  personSubs{
    action
    person{
      id
      documentNumber
      name
      photo
      lastnameOne
      lastnameTwo
      mobile
      email
      user{
        username
      }
      status
    }
  }
}
`
