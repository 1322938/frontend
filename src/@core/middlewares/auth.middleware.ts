import {
  AuthService
} from '@core/modules/auth'
import {
  SessionState
} from '@core/modules/auth/session.state'
import { notifyUtils } from '@core/modules/utils'
import { SocialService } from '@core/modules/social'

const sessionState = new SessionState()
const authService = new AuthService()
const _socialService = new SocialService()

export default async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const { isLogged, user: { username } } = sessionState.get()
    if (isLogged) {
      await _socialService.getPersons(username)
      next()
    } else {
      try {
        await authService.me()
        const { user: { username } } = sessionState.get()
        await _socialService.getPersons(username)
        next()
      } catch (error) {
        notifyUtils.error('session.errors.notValid')
        next({ name: 'SignIn' })
      }
    }
  } else if (to.matched.some(record => record.meta.nestedAuth)) {
    try {
      await authService.me()
      const { user: { username } } = sessionState.get()
      await _socialService.getPersons(username)
      next()
    } catch (error) {
      next()
    }
  } else {
    next()
  }
}
