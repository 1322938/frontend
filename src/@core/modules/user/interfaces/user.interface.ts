import { Person } from '@core/modules/person/interfaces/person.interface'
export interface User {
    username: string | null;
    userKind?: UserKind;
    person?: Person;
    lastSesion?: string|null;
    status?: boolean;
    verified?: boolean;
}

export interface UserKind {
    id: number | null;
    name: string | null;
}
