import { User, UserKind } from './interfaces/user.interface'

export {
  User,
  UserKind
}
