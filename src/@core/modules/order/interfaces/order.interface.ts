import { Locate } from '../../locate'
import { Buy } from '../../buy'
import { Sale } from '../../sale'

export interface Order {
    id?: number|null;
    status?: boolean;
    locate: Locate;
    quantity: number|string;
    price: number|string;
    buy: Buy;
    sale: Sale;
    created?: string;
}
