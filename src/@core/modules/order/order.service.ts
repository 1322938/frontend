import { OrderRepositoy } from './order.repository'

export class OrderService {
    private _orderRepository: OrderRepositoy

    constructor () {
      this._orderRepository = new OrderRepositoy()
    }

    async allOrders () {
      await this._orderRepository.allOrders()
    }

    async allOrdersPerson (person: number) {
      await this._orderRepository.allOrdersPerson(person)
    }

    async allOrdersBuy (buy: number) {
      return await this._orderRepository.allOrdersBuy(buy)
    }

    async changeOrdersSale (sale: number, status: string) {
      await this._orderRepository.changeOrdersSale(sale, status)
    }
}
