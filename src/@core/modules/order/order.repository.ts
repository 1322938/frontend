import { apolloClient } from '@/boot/apollo'
import { ALL_ORDERS, ALL_ORDERS_PERSON, ALL_ORDERS_BUY } from '@/@core/graphql/querys/server'
import { Order } from './interfaces/order.interface'
import { CLIENT_ALL_ORDERS } from '@/@core/graphql/querys/client'
import { CHANGE_STATUS_ORDERS_SALE_MUTATION } from '@/@core/graphql/mutations'

export class OrderRepositoy {
  async allOrders () {
    const { data: { allOrders } } = await apolloClient.query({
      query: ALL_ORDERS
    })
    this._setOrders(allOrders)
  }

  async allOrdersPerson (person: number) {
    const { data: { allOrdersPerson } } = await apolloClient.query({
      query: ALL_ORDERS_PERSON,
      variables: {
        person
      }
    })
    this._setOrders(allOrdersPerson)
  }

  async allOrdersBuy (buy: number) {
    const { data: { allOrdersBuy } } = await apolloClient.query({
      query: ALL_ORDERS_BUY,
      variables: {
        buy
      }
    })
    return allOrdersBuy
  }

  async changeOrdersSale (sale: number, status: string) {
    await apolloClient.mutate({
      mutation: CHANGE_STATUS_ORDERS_SALE_MUTATION,
      variables: {
        sale,
        status
      }
    })
  }

  _setOrders (orders: [Order]) {
    apolloClient.writeQuery({
      query: CLIENT_ALL_ORDERS,
      data: {
        orders
      }
    })
  }
}
