
export interface PersonSocial {
    person: number;
    documentNumber: string;
    photo?: string|null;
    fullname: string;
    mobile: string;
    email: string;
    username: string;
    status: boolean;
}

export interface ChatPersonSocial {
    channel: string;
    person: number;
    documentNumber: string;
    photo?: string|null;
    fullname: string;
    mobile: string;
    email: string;
    username: string;
    status: boolean;
}
