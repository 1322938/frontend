
export interface Message{
    id?: number;
    created?: string;
    channel: string;
    from: string;
    to: string;
    message: string;
}
