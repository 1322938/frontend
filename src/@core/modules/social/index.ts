import { PersonSocial, ChatPersonSocial } from './interfaces/person.social.interface'
import { Message } from './interfaces/message.social.interface'
import { SocialService } from './social.service'

export {
  PersonSocial,
  ChatPersonSocial,
  SocialService,
  Message
}
