import { apolloClient } from '@/boot/apollo'
import { ALL_PERSONS, GET_CHENNEL_USERS, ALL_CHATS_CHANNEL } from '@/@core/graphql/querys/server'
import { PersonSocial } from './'
import { CLIENT_PERSONS, CLIENT_FRIENDS } from '@/@core/graphql/querys/client/friend.query'
import { ALL_FRIENDS } from '@/@core/graphql/querys/server/friends.query'
import { ADD_FRIENDS_MUTATION, DELETE_FRIENDS_MUTATION } from '@/@core/graphql/mutations'
import { CLIENT_NEW_CHAT_PERSON, CLIENT_MESSAGES } from '@/@core/graphql/querys/client'
import { Message } from './interfaces/message.social.interface'
import { SEND_MESSAGE_CHAT_MUTATION } from '@/@core/graphql/mutations/chat.social.mutation'

export class SocialRepository {
  async getPersons () {
    const { data: { allPersons } } = await apolloClient.query({
      query: ALL_PERSONS
    })
    return allPersons
  }

  async getAllFriends (me: string) {
    const { data: { getFriends } } = await apolloClient.query({
      query: ALL_FRIENDS,
      variables: {
        me
      }
    })
    return getFriends
  }

  async addFriends (me: string, friend: string, person: PersonSocial) {
    await apolloClient.mutate({
      mutation: ADD_FRIENDS_MUTATION,
      variables: {
        me,
        friend
      },
      update: () => {
        const persons = this.getPersonsC()
        const friends = this.getFriendsC()
        const indexPersonUpdate = persons.findIndex(people => people.username === person.username)
        persons[indexPersonUpdate].status = true
        this.setPersons(persons)
        friends.push(persons[indexPersonUpdate])
        this.setFriends(friends)
      }
    })
  }

  async deleteFriends (me: string, friend: string, person: PersonSocial) {
    await apolloClient.mutate({
      mutation: DELETE_FRIENDS_MUTATION,
      variables: {
        me,
        friend
      },
      update: () => {
        const persons = this.getPersonsC()
        const friends = this.getFriendsC()
        const indexPersonUpdate = persons.findIndex(people => people.username === person.username)
        const indexFriendUpdate = friends.findIndex(friend => friend.username === person.username)
        persons[indexPersonUpdate].status = false
        this.setPersons(persons)
        friends.splice(indexFriendUpdate, 1)
        this.setFriends(friends)
      }
    })
  }

  async getChannelUsers (from: string, to: string) {
    const { data: { getChannelUser } } = await apolloClient.query({
      query: GET_CHENNEL_USERS,
      variables: {
        from,
        to
      }
    })
    return getChannelUser
  }

  async sendMessageChat (messages: Message) {
    await apolloClient.mutate({
      mutation: SEND_MESSAGE_CHAT_MUTATION,
      variables: {
        ...messages
      }
    })
  }

  async getAllChatsChannel (channel: string) {
    const { data: { allChatsChannel } } = await apolloClient.query({
      query: ALL_CHATS_CHANNEL,
      variables: {
        channel
      }
    })
    this.setMessagesC(allChatsChannel)
  }

  setMessagesC (messages: [Message]) {
    apolloClient.writeQuery({
      query: CLIENT_MESSAGES,
      data: {
        messages
      }
    })
  }

  getMessagesC () {
    const { messages } = apolloClient.readQuery({
      query: CLIENT_MESSAGES
    })
    return messages
  }

  getPersonsC () {
    const { persons } = apolloClient.readQuery({
      query: CLIENT_PERSONS
    })
    return persons
  }

  getFriendsC () {
    const { friends } = apolloClient.readQuery({
      query: CLIENT_FRIENDS
    })
    return friends
  }

  setPersons (persons: [PersonSocial]) {
    apolloClient.writeQuery({
      query: CLIENT_PERSONS,
      data: {
        persons
      }
    })
  }

  setFriends (friends: [PersonSocial]) {
    apolloClient.writeQuery({
      query: CLIENT_FRIENDS,
      data: {
        friends
      }
    })
  }

  setPersonChat (person: PersonSocial) {
    apolloClient.writeQuery({
      query: CLIENT_NEW_CHAT_PERSON,
      data: {
        startChatPerson: person
      }
    })
  }
}
