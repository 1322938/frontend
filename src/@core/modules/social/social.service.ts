import { SocialRepository } from './social.repository'
import { PersonSocial } from './'
import { person } from '@/@core/cache'
import { SessionState } from '../auth/session.state'
import { Message } from './interfaces/message.social.interface'

export class SocialService {
    private _socialRepository: SocialRepository
    private _authRepository: SessionState

    constructor () {
      this._socialRepository = new SocialRepository()
      this._authRepository = new SessionState()
    }

    async getPersons (user: string) {
      const persons = await this._socialRepository.getPersons()
      let mefriends
      try {
        mefriends = await this._socialRepository.getAllFriends(user)
      } catch (error) {
        mefriends = {}
      }
      const sortedFriends: [PersonSocial] = [person]
      const sortedPersons: [PersonSocial] = [person]
      persons.map(element => {
        if (user !== element.user.username) {
          let statusfriend = false
          if (Object.entries(mefriends).length !== 0) {
            const { friends } = mefriends
            friends.includes(element.user.username) ? statusfriend = true : statusfriend = false
          }
          const person: PersonSocial = {
            person: Number(element.id),
            documentNumber: element.documentNumber,
            fullname: element.name + ' ' + element.lastnameOne + ' ' + element.lastnameTwo,
            mobile: element.mobile,
            email: element.email,
            photo: element.photo,
            username: element.user.username,
            status: statusfriend
          }
          if (person.status) sortedFriends.push(person)
          sortedPersons.push(person)
        }
      })
      this._socialRepository.setFriends(sortedFriends)
      this._socialRepository.setPersons(sortedPersons)
    }

    async addFriends (me: string, friend: PersonSocial) {
      await this._socialRepository.addFriends(me, friend.username, friend)
    }

    async deleteFriends (me: string, friend: PersonSocial) {
      await this._socialRepository.deleteFriends(me, friend.username, friend)
    }

    async startChat (person: PersonSocial) {
      const fromuser = this._authRepository.get()
      const { id } = await this._socialRepository.getChannelUsers(fromuser.user.username, person.username)
      const newChatUsers = {
        channel: id,
        ...person
      }
      this._socialRepository.setPersonChat(newChatUsers)
    }

    async sendMessage (message: Message) {
      await this._socialRepository.sendMessageChat(message)
    }

    async getAllChatsChannel (channel: string) {
      await this._socialRepository.getAllChatsChannel(channel)
    }
}
