import { apolloClient } from '@/boot/apollo'
import { CREATE_REPORT_MUTATION, UPDATE_REPORT_MUTATION, DELETE_REPORT_MUTATION, ADD_RESPONSE_RESPORT_MUTATION } from '@/@core/graphql/mutations/report.mutation'
import { ALL_REPORTS, ONE_REPORT_ID } from '@/@core/graphql/querys/server'

export class ReportService {
  async createReport (data) {
    await apolloClient.mutate({
      mutation: CREATE_REPORT_MUTATION,
      variables: {
        ...data
      }
    })
  }

  async updateReport (id, data) {
    await apolloClient.mutate({
      mutation: UPDATE_REPORT_MUTATION,
      variables: {
        id,
        ...data
      }
    })
  }

  async deleteReport (id) {
    await apolloClient.mutate({
      mutation: DELETE_REPORT_MUTATION,
      variables: {
        id
      }
    })
  }

  async addResposeReport (id, user, response) {
    await apolloClient.mutate({
      mutation: ADD_RESPONSE_RESPORT_MUTATION,
      variables: {
        id,
        user,
        response
      }
    })
  }

  async allReports () {
    const { data: { allReports } } = await apolloClient.query({
      query: ALL_REPORTS
    })
    return allReports
  }

  async oneReportId (id: number) {
    const { data: { oneReportID } } = await apolloClient.query({
      query: ONE_REPORT_ID,
      variables: {
        id
      }
    })
    return oneReportID
  }
}
