import { Locate } from './interfaces/locate.interface'
import { LocateService } from './locate.service'

export {
  Locate,
  LocateService
}
