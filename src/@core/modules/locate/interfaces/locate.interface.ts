
export interface Locate{
    id?: number;
    latitud: string;
    longitud: string;
    observation?: string| null;
    status?: boolean;
}
