
export class LocateService {
  isMarkerInsidePolygon (marker, poly) {
    let inside = false
    inside = poly.getLatLng().distanceTo(marker.getLatLng()) < poly.getRadius()

    if (!inside) {
      throw new Error('Error marcador fuera de rango')
    }

    return inside
  };
}
