import { District } from '../../general'
import { User } from '../../user'

export interface AreaInput{
    id?: number|null;
    name: string;
    latitud: number;
    longitud: number;
    radio?: number;
    user?: User | string;
    district?: District| number;
    observation?: string;
}
