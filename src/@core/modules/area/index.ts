import { AreaInput } from './interfaces/area.interface'
import { AreaService } from './area.service'

export {
  AreaInput,
  AreaService
}
