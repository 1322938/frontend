import { AreaInput } from './'
import { apolloClient } from '@/boot/apollo'
import { CREATE_AREA_MUTATION } from '@/@core/graphql/mutations'
import { ALL_AREAS } from '@/@core/graphql/querys/server'
import { CLIENT_AREA, CLIENT_AREAS } from '@/@core/graphql/querys/client/area.query'

export class AreaRepository {
  async createArea (data: AreaInput) {
    const { data: { createArea } } = await apolloClient.mutate({
      mutation: CREATE_AREA_MUTATION,
      variables: {
        ...data
      }
    })
    this.setArea(createArea)
  }

  async allAreas () {
    const { data: { allAreas } } = await apolloClient.query({
      query: ALL_AREAS
    })
    this.setAreas(allAreas)
  }

  setAreas (areas: [AreaInput]) {
    apolloClient.writeQuery({
      query: CLIENT_AREAS,
      data: {
        areas
      }
    })
  }

  setArea (area: AreaInput) {
    apolloClient.writeQuery({
      query: CLIENT_AREA,
      data: {
        area
      }
    })
  }
}
