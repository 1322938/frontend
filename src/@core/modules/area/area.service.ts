import { AreaRepository } from './area.repository'
import { AreaInput } from '.'

export class AreaService {
    private _areaRepository: AreaRepository

    constructor () {
      this._areaRepository = new AreaRepository()
    }

    async createArea (data: AreaInput) {
      await this._areaRepository.createArea(data)
    }

    async allAreas () {
      await this._areaRepository.allAreas()
    }
}
