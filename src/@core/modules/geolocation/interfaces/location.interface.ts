import { User } from '../../user'
import { District } from '../../general'

export interface Location{
    id?: number|null;
    latitud: number;
    longitud: number;
    user: User | string;
    district: District| number;
    observation?: string;
}
