import { Payment } from '../../payment'
import { Person } from '../../person'

export interface Buy {
    id?: number;
    payment: Payment|number;
    person?: Person|number;
    observation?: string;
    status?: boolean;
    created?: string;
}
