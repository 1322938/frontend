import { Buy } from './interfaces/buy.interafce'
import { apolloClient } from '@/boot/apollo'
import { CREATE_BUY_MUTATION } from '@/@core/graphql/mutations'
import { ALL_BUYS_PERSON } from '@/@core/graphql/querys/server'
import { CLIENT_ALL_BUYS } from '@/@core/graphql/querys/client'

export class BuyRepository {
  async createBuy (data: Buy) {
    const { data: { createBuy } } = await apolloClient.mutate({
      mutation: CREATE_BUY_MUTATION,
      variables: {
        ...data
      }
    })
    return createBuy
  }

  async allBuysPerson (person: number) {
    const { data: { allBuysPerson } } = await apolloClient.query({
      query: ALL_BUYS_PERSON,
      variables: {
        person
      }
    })
    this._setBuys(allBuysPerson)
  }

  _setBuys (buys: [Buy]) {
    apolloClient.writeQuery({
      query: CLIENT_ALL_BUYS,
      data: {
        buys
      }
    })
  }
}
