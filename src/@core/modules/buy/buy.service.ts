import { BuyRepository } from './buy.repository'

export class BuyService {
    private _buyRepository: BuyRepository

    constructor () {
      this._buyRepository = new BuyRepository()
    }

    async allBuysPerson (person: number) {
      await this._buyRepository.allBuysPerson(person)
    }
}
