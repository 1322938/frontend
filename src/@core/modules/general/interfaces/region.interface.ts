export interface Region{
    id?: number | null;
    name: string;
    description: string | null;
    ubigeo: string | null;
    status: boolean;
}
