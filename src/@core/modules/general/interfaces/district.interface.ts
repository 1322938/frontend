import { Province } from './province.interface'

export interface District {
    id?: number | null;
    province?: Province;
    name: string | null;
    description: string | null;
    ubigeo: string | null;
    status: boolean | false;
}
