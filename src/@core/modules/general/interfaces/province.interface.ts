import { Region } from './region.interface'

export interface Province{
    id?: number | null;
    name: string;
    description: string | null;
    ubigeo: string | null;
    status: boolean;
    region?: Region;
}
