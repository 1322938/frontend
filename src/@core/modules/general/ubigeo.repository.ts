import { apolloClient } from '@/boot/apollo'
import {
  GET_REGIONS,
  GET_PROVINCES_REGION,
  GET_DISTRICTS_PROVINCE
} from '@/@core/graphql/querys/server'

export class UbigeoRepository {
  async getRegions () {
    const {
      data: { allRegions }
    } = await apolloClient.query({
      query: GET_REGIONS
    })
    return { allRegions }
  }

  async getProvincesRegion (region) {
    const {
      data: { allProvincesRegion }
    } = await apolloClient.query({
      query: GET_PROVINCES_REGION,
      variables: { region }
    })
    return { allProvincesRegion }
  }

  async getDistrictsProvince (province) {
    const {
      data: { allDistritsProvince }
    } = await apolloClient.query({
      query: GET_DISTRICTS_PROVINCE,
      variables: { province }
    })
    return { allDistritsProvince }
  }
}
