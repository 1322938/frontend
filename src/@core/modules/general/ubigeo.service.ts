import { UbigeoRepository } from './ubigeo.repository'

export class UbigeoService {
  private _ubigeoRepository: UbigeoRepository;

  constructor () {
    this._ubigeoRepository = new UbigeoRepository()
  }

  async getRegions () {
    return await this._ubigeoRepository.getRegions()
  }

  async getProvincesRegion (region) {
    return await this._ubigeoRepository.getProvincesRegion(region)
  }

  async getDistrictsProvince (province) {
    return await this._ubigeoRepository.getDistrictsProvince(province)
  }
}
