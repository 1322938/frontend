import { UbigeoService } from './ubigeo.service'
import { Region } from './interfaces/region.interface'
import { Province } from './interfaces/province.interface'
import { District } from './interfaces/district.interface'

export { UbigeoService, Region, Province, District }
