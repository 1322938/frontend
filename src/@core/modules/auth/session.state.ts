import { StorageState } from './storage.state'
import { apolloClient } from '@/boot/apollo'
import { CLIENT_SESSION_QUERY } from '@/@core/graphql/querys/client/session.query'
import { Session } from './interfaces/session.interface'
import { session } from '@core/cache'
import { SET_SESSION_MUTATION } from '@/@core/graphql/mutations'

export class SessionState {
    private _storageState: StorageState
    // eslint-disable-next-line no-useless-constructor
    constructor () {
      this._storageState = new StorageState()
    }

    get () {
      const {
        session: { isLogged, user, token }
      } = apolloClient.readQuery({
        query: CLIENT_SESSION_QUERY
      })

      return {
        isLogged,
        user,
        token
      }
    }

    set ({ user, token }: Session) {
      apolloClient.mutate({
        mutation: SET_SESSION_MUTATION,
        update: (cache) => {
          cache.writeQuery({
            query: CLIENT_SESSION_QUERY,
            data: {
              session: {
                isLogged: true,
                user
              }
            }
          })
        }
      })

      if (token) {
        this._storageState.setToken(token)
      }
    }

    close () {
      apolloClient.resetStore()
      apolloClient.clearStore()

      apolloClient.mutate({
        mutation: SET_SESSION_MUTATION,
        update (cache) {
          cache.writeQuery({
            query: CLIENT_SESSION_QUERY,
            data: {
              session: {
                isLogged: false,
                user: session
              }
            }
          })
        }
      })
      window.location.reload()
      this._storageState.purgeToken()
    }
}
