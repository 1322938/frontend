export interface Register {
    username: string;
    password: string;
    passwordConfirmation: string;
    documentNumber: string;
    name: string;
    lastnameOne: string;
    lastnameTwo: string;
    gender: string;
    phone: string | null;
    mobile: string;
    email: string;
    district: string;
    address: string;
    birthdate: string;
    response: string;
}
