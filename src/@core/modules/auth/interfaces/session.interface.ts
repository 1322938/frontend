import { User } from '@core/modules/user/interfaces/user.interface'

export interface Session {
    user?: User;
    isLogged?: boolean;
    token?: string;
}
