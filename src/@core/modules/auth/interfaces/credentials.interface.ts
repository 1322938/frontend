export interface Credentials {
    username: string;
    password: string;
    response: string;
}
