import { apolloClient } from '@/boot/apollo'
import { ME_QUERY } from '@core/graphql/querys/server'
import { SessionState } from './session.state'
import { Credentials } from './interfaces/credentials.interface'
import { SIGN_IN_MUTATION, SIGN_UP_MUTATION } from '@/@core/graphql/mutations'
import { Register } from './interfaces/register.interfaces'
import { CLIENT_SESSION_QUERY } from '@/@core/graphql/querys/client/session.query'
import { StorageState } from './storage.state'

export class AuthRepository {
  private _sessionState: SessionState;
  private _storageState: StorageState
  // eslint-disable-next-line no-useless-constructor
  constructor () {
    this._sessionState = new SessionState()
    this._storageState = new StorageState()
  }

  async me () {
    const {
      data: { me }
    } = await apolloClient.query({
      query: ME_QUERY
    })
    this._sessionState.set(me)
  }

  async signIn (data: Credentials) {
    await apolloClient.mutate({
      mutation: SIGN_IN_MUTATION,
      variables: {
        ...data
      },
      update: (cache, { data: { signIn: { user, token } } }) => {
        const isLogged = true
        cache.writeQuery({
          query: CLIENT_SESSION_QUERY,
          data: {
            session: {
              isLogged,
              user
            }
          }
        })
        this._storageState.setToken(token)
      }
    })
  }

  async signUp (data: Register) {
    const {
      data: { signUp }
    } = await apolloClient.mutate({
      mutation: SIGN_UP_MUTATION,
      variables: {
        ...data
      }
    })
    this._sessionState.set(signUp)
  }
}
