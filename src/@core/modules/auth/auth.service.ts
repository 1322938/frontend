import { AuthRepository } from './auth.repository'
import { Credentials } from './interfaces/credentials.interface'
import { Register } from './interfaces/register.interfaces'

import axios from 'axios'
import * as dotenv from 'dotenv'

dotenv.config()

export class AuthService {
  private _authRepository: AuthRepository;
  // eslint-disable-next-line no-useless-constructor
  constructor () {
    this._authRepository = new AuthRepository()
  }

  async me () {
    await this._authRepository.me()
  }

  async signIn (data: Credentials) {
    await this._authRepository.signIn(data)
  }

  async signUp (data: Register) {
    await this._authRepository.signUp(data)
  }

  async sendEmail (email: string) {
    await axios.post(process.env.API_REST_URL + 'auth/', {
      email
    })
  }

  async userVerified (token: string) {
    await axios.get(process.env.API_REST_URL + 'auth/' + token)
  }
}
