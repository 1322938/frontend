export class StorageState {
  setToken (token: string) { return localStorage.setItem('token', token) }

  getToken () { return localStorage.getItem('token') }

  purgeToken () { return localStorage.removeItem('token') }
}
