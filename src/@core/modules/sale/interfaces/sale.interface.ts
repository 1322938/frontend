
import { AreaInput } from '../../area'
import { Product } from '../../product/interfaces/product.interface'
import { Person } from '../../person'

export interface Sale{
    id?: number;
    person?: number|Person;
    product: number|Product;
    unitPrice: number;
    stock: number;
    weight: number;
    area: number|AreaInput;
    observation?: string|null;
}
