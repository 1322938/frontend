import { AreaInput } from '../../area'

export interface InputSale{
    id?: number;
    person: number;
    product: number;
    unitPrice: number;
    stock: number;
    area: AreaInput|number;
    weight: number;
    observation?: string|null;
}
