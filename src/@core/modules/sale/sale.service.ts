import { SaleRepository } from './sale.repository'
import { InputSale } from './interfaces/input.sale.interface'

export class SaleService {
    private _saleRepository: SaleRepository

    constructor () {
      this._saleRepository = new SaleRepository()
    }

    async createSale (data: InputSale) {
      const { stock, unitPrice, weight, ...rest } = data
      const newStock = Number(stock)
      const newUniteprice = Number(unitPrice)
      const newWeigth = Number(weight)
      await this._saleRepository.createSale({ stock: newStock, unitPrice: newUniteprice, weight: newWeigth, ...rest })
    }

    async updateSale (id: number, data: InputSale) {
      const { unitPrice, weight, ...rest } = data
      await this._saleRepository.updateSale(id, { unitPrice: Number(unitPrice), weight: Number(weight), ...rest })
    }

    async deleteSale (id: number) {
      await this._saleRepository.deleteSale(id)
    }

    async getSales () {
      await this._saleRepository.getSales()
    }

    async getSalesPerson (person: number) {
      await this._saleRepository.getSalesPerson(person)
    }

    async getSaleID (id: number) {
      await this._saleRepository.getSaleID(id)
    }
}
