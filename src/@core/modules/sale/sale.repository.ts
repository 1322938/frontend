import { InputSale } from './interfaces/input.sale.interface'
import { apolloClient } from '@/boot/apollo'
import { CREATE_SALE_MUTATION, UPDATE_SALE_MUTATION, DELETE_SALE_MUTATION } from '@/@core/graphql/mutations'
import { ALL_SALES, ONE_SALE, ALL_SALES_PERSON } from '@/@core/graphql/querys/server'
import { Sale } from './interfaces/sale.interface'
import { CLIENT_SALES, CLIENT_ONE_SALE } from '@/@core/graphql/querys/client/sale.query'

export class SaleRepository {
  async createSale (data: InputSale) {
    await apolloClient.mutate({
      mutation: CREATE_SALE_MUTATION,
      variables: {
        ...data
      },
      update: (cache, { data: { createSale } }) => {
        const sales = this._getSales()
        sales.unshift(createSale)
        this._setSales(sales)
      }
    })
  }

  async updateSale (id, data: InputSale) {
    await apolloClient.mutate({
      mutation: UPDATE_SALE_MUTATION,
      variables: {
        ...data,
        id
      },
      update: (cache, { data: { updateSale } }) => {
        const sales = this._getSales()
        const indexSaleUpdate = sales.findIndex(sale => sale.id === updateSale.id)
        sales[indexSaleUpdate] = updateSale
        this._setSales(sales)
      }
    })
  }

  async deleteSale (id) {
    await apolloClient.mutate({
      mutation: DELETE_SALE_MUTATION,
      variables: {
        id
      },
      update: (cache, { data: { deleteSale } }) => {
        const sales = this._getSales()
        const indexSaleUpdate = sales.findIndex(sale => sale.id === deleteSale.id)
        sales.splice(indexSaleUpdate, 1)
        this._setSales(sales)
      }
    })
  }

  async getSales () {
    const {
      data: { allSales }
    } = await apolloClient.query({
      query: ALL_SALES
    })
    this._setSales(allSales)
  }

  async getSaleID (id: number) {
    const { data: { oneSaleId } } = await apolloClient.query({
      query: ONE_SALE,
      variables: {
        id
      }
    })
    this._setSale(oneSaleId)
  }

  async getSalesPerson (person: number) {
    const { data: { allSalesPerson } } = await apolloClient.query({
      query: ALL_SALES_PERSON,
      variables: {
        person
      }
    })
    this._setSales(allSalesPerson)
  }

  _setSale (sale: Sale) {
    apolloClient.writeQuery({
      query: CLIENT_ONE_SALE,
      data: {
        sale
      }
    })
  }

  _setSales (data: [Sale]) {
    apolloClient.writeQuery({
      query: CLIENT_SALES,
      data: {
        sales: data
      }
    })
  }

  _getSales () {
    const { sales } = apolloClient.readQuery({
      query: CLIENT_SALES
    })
    return sales
  }
}
