import { apolloClient } from '@/boot/apollo'
import { ALL_PRODUCTS } from '@core/graphql/querys/server'
import { Product } from './interfaces/product.interface'
import { CLIENT_PRODUCTS } from '@/@core/graphql/querys/client/product.query'

export class ProductRepository {
  async getProducts () {
    const { data: { allProducts } } = await apolloClient.query({
      query: ALL_PRODUCTS
    })
    this._setProduct(allProducts)
  }

  _setProduct (data: [Product]) {
    apolloClient.writeQuery({
      query: CLIENT_PRODUCTS,
      data: {
        products: data
      }
    })
  }
}
