import { KindRepository } from './kind.repository'

export class KindService {
  private _kindRepository: KindRepository;

  constructor () {
    this._kindRepository = new KindRepository()
  }

  async getProductKinds () {
    return await this._kindRepository.getProductKinds()
  }
}
