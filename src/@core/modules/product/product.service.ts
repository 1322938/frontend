import { ProductRepository } from './product.repository'

export class ProductService {
    private _productRepository: ProductRepository

    constructor () {
      this._productRepository = new ProductRepository()
    }

    async getProducts () {
      await this._productRepository.getProducts()
    }
}
