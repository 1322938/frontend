import { Category, Kind } from '..'

export interface Product{
    id?: number;
    category: number|Category;
    kind: number|Kind;
    name: string;
    description?: string|null;
    status?: boolean;
}
