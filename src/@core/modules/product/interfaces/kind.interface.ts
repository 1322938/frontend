
export interface Kind{
    id?: number;
    name: string;
    description?: string|null;
    status?: boolean;
}
