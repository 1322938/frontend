import { apolloClient } from '@/boot/apollo'
import {
  ALL_PRODUCT_CATEGORYS
} from '@core/graphql/querys/server'

export class CategoryRepository {
  async getProductCategorys () {
    const {
      data: { allProductCategorys }
    } = await apolloClient.query({
      query: ALL_PRODUCT_CATEGORYS
    })
    return allProductCategorys
  }
}
