import { CategoryRepository } from './category.repository'

export class CategoryService {
  private _categoryRepository: CategoryRepository;

  constructor () {
    this._categoryRepository = new CategoryRepository()
  }

  async getProductCategorys () {
    return await this._categoryRepository.getProductCategorys()
  }
}
