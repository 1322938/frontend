import { apolloClient } from '@/boot/apollo'
import {
  ALL_PRODUCT_KINDS
} from '@core/graphql/querys/server'

export class KindRepository {
  async getProductKinds () {
    const {
      data: { allProductKinds }
    } = await apolloClient.query({
      query: ALL_PRODUCT_KINDS
    })
    return allProductKinds
  }
}
