import { ProductService } from './product.service'
import { CategoryService } from './category.service'
import { KindService } from './kind.service'
import { Category } from './interfaces/category.interafce'
import { Kind } from './interfaces/kind.interface'

export { ProductService, CategoryService, KindService, Category, Kind }
