import { apolloClient } from '@/boot/apollo'
import { CLIENT_CAR } from '@/@core/graphql/querys/client/shopping.car.query'
import { Car } from '.'

export class ShoppingCarRepository {
  set (shoppingcar: Car) {
    apolloClient.writeQuery({
      query: CLIENT_CAR,
      data: {
        shoppingcar
      }
    })
  }

  get () {
    const { shoppingcar } = apolloClient.readQuery({
      query: CLIENT_CAR
    })
    return shoppingcar
  }
}
