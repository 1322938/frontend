// import { Product } from '../../product/interfaces/product.interface'
// import { AreaInput } from '../../area'
import { Sale } from '../../sale'
import { Locate } from '../../locate'

// export interface Car{
//     id?: number;
//     product: number|Product;
//     unitPrice: number;
//     stock: number;
//     area?: number|AreaInput;
//     observation?: string|null;
// }

export interface ProductCar{
    sale: Sale;
    quantity: number;
    unitPrice: number;
    process?: string|null;
    location?: Locate|null;
    subtotal: number;
}

export interface Car{
    product: [ProductCar];
    igv: number;
    commission: number;
    total: number;
}
