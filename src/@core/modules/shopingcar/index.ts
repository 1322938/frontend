import { ShoppingCarService } from './car.service'
import { Car } from './interfaces/car.interface'

export {
  ShoppingCarService,
  Car
}
