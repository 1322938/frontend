import { ShoppingCarRepository } from './car.repository'
import { InputSale } from '../sale'
import { ProductCar, Car } from './interfaces/car.interface'
import { Locate } from '../locate'
import { shoppingcar } from '@/@core/cache'

export class ShoppingCarService {
  private _shoppingRepository: ShoppingCarRepository;

  constructor () {
    this._shoppingRepository = new ShoppingCarRepository()
  }

  addProduct (data: InputSale, quantity?: number | null) {
    const car = this._shoppingRepository.get()
    const { product } = car

    const productIndex = product.findIndex(
      productcar => productcar.sale.id === data.id
    )

    const lengthProduct = product.length

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let sumTot = 0

    let newProduct: ProductCar

    const newLocate: Locate = {
      id: 0,
      latitud: '0',
      longitud: '0',
      status: true
    }

    if (productIndex !== -1) {
      newProduct = product[productIndex]
      newProduct.sale = data
      newProduct.quantity = !quantity ? newProduct.quantity + 1 : quantity
      newProduct.subtotal = data.unitPrice * newProduct.quantity
      newProduct.unitPrice = data.unitPrice
      newProduct.location = newLocate
      sumTot = newProduct.subtotal
      product[productIndex] = newProduct
    } else {
      newProduct = {
        sale: data,
        quantity: 1,
        unitPrice: data.unitPrice,
        subtotal: data.unitPrice,
        location: newLocate
      }

      if (lengthProduct === 1) {
        product[0].subtotal === 0
          ? (product[0] = newProduct)
          : product.push(newProduct)
      } else {
        product.push(newProduct)
      }
      const { sumTotal } = this.calcCarTotal(product)
      sumTot = sumTotal
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { sumTotal, ...rest } = this.calcCarTotal(product)
    const newCar: Car = {
      ...rest,
      product
    }
    this._shoppingRepository.set(newCar)
  }

  calcCarTotal (product) {
    let sumTotal = 0
    let igv = 0
    let commission = 0
    product.forEach(element => {
      sumTotal += element.subtotal
    })

    igv = Math.round(sumTotal * 0.18 * 100) / 100
    commission = Math.round((sumTotal * 0.042 * 1.07 + 0.042) * 100) / 100
    const total = Math.round((sumTotal + commission + igv) * 100) / 100

    return { sumTotal, igv, commission, total }
  }

  deleteSale (id: number) {
    const car = this._shoppingRepository.get()
    const { product } = car
    const productIndex = product.findIndex(
      productcar => productcar.sale.id === id
    )

    product.splice(productIndex, 1)
    if (product.length !== 0) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { sumTotal, ...rest } = this.calcCarTotal(product)
      const newCar: Car = {
        ...rest,
        product
      }
      this._shoppingRepository.set(newCar)
    } else {
      /** length 0 */
      this._shoppingRepository.set(shoppingcar)
    }
  }

  editSale (id: number, quantity?: number) {
    const car = this._shoppingRepository.get()
    const { product } = car

    const productIndex = product.findIndex(
      productcar => productcar.sale.id === id
    )

    let res

    if (quantity) {
      product[productIndex].quantity = quantity
      product[productIndex].subtotal = quantity * product[productIndex].unitPrice

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { sumTotal, ...restTwo } = this.calcCarTotal(product)
      res = restTwo
    }

    const newCar: Car = {
      ...res,
      product
    }

    this._shoppingRepository.set(newCar)
  }

  editSaleLocation (id, locate?: Locate|null) {
    const car = this._shoppingRepository.get()
    const { product, ...rest } = car

    const productIndex = product.findIndex(
      productcar => productcar.sale.id === id
    )

    if (locate) {
      product[productIndex].location = locate
    }

    const newCar: Car = {
      ...rest,
      product
    }

    this._shoppingRepository.set(newCar)
  }
}
