export interface Person {
    id?: number | null;
    name: string | null;
    lastnameOne: string | null;
    lastnameTwo: string | null;
    documentNumber?: string | null;
    email?: string | null;
}
