import { Person } from './interfaces/person.interface'
import { PersonService } from './person.service'

export {
  Person,
  PersonService
}
