import axios from 'axios'
import * as dotenv from 'dotenv'

dotenv.config()

export class PersonService {
  async sendPhoto (id: number, file) {
    const formData = new FormData()
    formData.append('id', id.toString())
    formData.append('file', file)
    await axios.post(process.env.API_REST_URL + 'person/avatar', formData)
  }
}
