import { PaymentRepository } from './payment.repository'
import { Culqi } from '.'
import { ShoppingCarRepository } from '../shopingcar/car.repository'

export class PaymentService {
    private _paymentRepository: PaymentRepository
    private _shopingCarRepository: ShoppingCarRepository

    constructor () {
      this._paymentRepository = new PaymentRepository()
      this._shopingCarRepository = new ShoppingCarRepository()
    }

    async paymentCulqi (data: Culqi) {
      const car = await this._shopingCarRepository.get()
      await this._paymentRepository.paymentCarMapping(data, car)
    }
}
