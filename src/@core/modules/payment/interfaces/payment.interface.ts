
export interface Payment {
    id?: number;
    igv: number;
    commission: number;
    total: number;
    status?: boolean;
}

export interface Culqi{
    amount: number;
    currency_code: string;
    email: string;
    souce_id: string;
}
