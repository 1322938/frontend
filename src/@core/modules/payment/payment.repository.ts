import { Culqi, Payment } from './interfaces/payment.interface'
import { apolloClient } from '@/boot/apollo'
import { CULQI_MUTATION, CREATE_PAYMENT_MUTATION, PAYMENT_CAR_MAPPING_MUTATION } from '@/@core/graphql/mutations/payment.mutation'
import { Car } from '../shopingcar'

export class PaymentRepository {
  async paymentCulqi (data: Culqi) {
    const { data: { culqiPay: { payment } } } = await apolloClient.mutate({
      mutation: CULQI_MUTATION,
      variables: {
        ...data
      }
    })
    return payment
  }

  async payProducts (data: Payment) {
    const { data: { createPayment } } = await apolloClient.mutate({
      mutation: CREATE_PAYMENT_MUTATION,
      variables: {
        ...data
      }
    })
    return createPayment
  }

  async paymentCarMapping (culqi: Culqi, car: Car) {
    await apolloClient.mutate({
      mutation: PAYMENT_CAR_MAPPING_MUTATION,
      variables: {
        car,
        culqi
      }
    })
  }
}
