import { Region, Province, District } from '@core/modules/general'
import { User } from '@core/modules/user'
import { AreaInput } from '@/@core/modules/area'
import { Person } from '@/@core/modules/person'

export default {
  id: 0,
  name: '',
  latitud: 0,
  longitud: 0,
  radio: 0,
  district: {
    id: 0,
    name: '',
    province: {
      id: 0,
      name: '',
      region: {
        id: 0,
        name: ''
      } as Region
    } as Province
  } as District,
  user: {
    username: '',
    person: {
      id: 0,
      name: '',
      lastnameOne: '',
      lastnameTwo: ''
    } as Person
  } as User,
  status: false
} as AreaInput
