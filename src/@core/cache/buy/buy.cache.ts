import { Payment } from '@/@core/modules/payment'
import { Buy } from '@/@core/modules/buy'

export default {
  id: 0,
  payment: {
    id: 0,
    igv: 0,
    commission: 0,
    total: 0,
    status: false
  } as Payment,
  observation: '',
  status: false,
  created: ''
}as Buy
