import { Locate } from '@/@core/modules/locate'
import { Person } from '@/@core/modules/person'
import { Buy } from '@/@core/modules/buy'
import { AreaInput } from '@/@core/modules/area'
import { Category, Kind } from '@/@core/modules/product'
import { Product } from '@/@core/modules/product/interfaces/product.interface'

export default {
  id: '',
  status: '',
  created: '',
  locate: {
    id: 0,
    latitud: '',
    longitud: ''
  } as Locate,
  quantity: '',
  price: '',
  process: '',
  buy: {
    id: 0,
    person: {
      id: 0,
      documentNumber: '',
      name: '',
      lastnameOne: '',
      lastnameTwo: ''
    }as Person
  }as Buy,
  sale: {
    id: 0,
    unitPrice: '',
    stock: '',
    weight: '',
    area: {
      id: 0,
      name: '',
      latitud: 0,
      longitud: 0,
      radio: 0
    } as AreaInput,
    product: {
      id: 0,
      name: '',
      description: '',
      category: {
        id: 0,
        name: ''
      } as Category,
      kind: {
        id: 0,
        name: ''
      } as Kind
    }as Product
  }
}
