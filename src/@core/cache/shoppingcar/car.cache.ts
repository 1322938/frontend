import { Product } from '@/@core/modules/product/interfaces/product.interface'
import { Category, Kind } from '@/@core/modules/product'
import { Car } from '@/@core/modules/shopingcar'
import { Sale } from '@/@core/modules/sale'
import { ProductCar } from '@/@core/modules/shopingcar/interfaces/car.interface'
import { Locate } from '@/@core/modules/locate'
import { AreaInput } from '@/@core/modules/area'

// export default {
//   id: 0,
//   product: {
//     id: 0,
//     name: '',
//     category: {
//       id: 0,
//       name: ''
//     } as Category,
//     kind: {
//       id: 0,
//       name: ''
//     } as Kind
//   } as Product,
//   unitPrice: 0,
//   stock: 0
// } as Car

export default {
  igv: 0,
  commission: 0,
  total: 0,
  product: [
    {
      sale: {
        id: 0,
        stock: 0,
        weight: 0,
        area: {
          id: 0,
          name: '',
          latitud: 0,
          longitud: 0,
          radio: 0
        } as AreaInput,
        product: {
          id: 0,
          name: '',
          category: {
            id: 0,
            name: ''
          } as Category,
          kind: {
            id: 0,
            name: ''
          } as Kind
        } as Product
      } as Sale,
      quantity: 0,
      subtotal: 0,
      unitPrice: 0,
      location: {
        id: 0,
        latitud: '',
        longitud: '',
        status: false
      } as Locate
    } as ProductCar
  ]
} as Car
