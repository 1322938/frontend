import { Sale } from '@/@core/modules/sale'
import { Product } from '@/@core/modules/product/interfaces/product.interface'
import { AreaInput } from '@/@core/modules/area'
import { Category, Kind } from '@/@core/modules/product'
import { District, Province, Region } from '@/@core/modules/general'
import { Person } from '@/@core/modules/person'
import { User } from '@/@core/modules/user'

export default {
  id: 0,
  person: {
    id: 0,
    name: '',
    user: {
      username: ''
    } as User,
    lastnameOne: '',
    lastnameTwo: ''
  } as Person,
  product: {
    id: 0,
    name: '',
    category: {
      id: 0,
      name: ''
    } as Category,
    kind: {
      id: 0,
      name: ''
    } as Kind
  } as Product,
  unitPrice: 0,
  stock: 0,
  weight: 0,
  area: {
    id: 0,
    name: '',
    latitud: 0,
    longitud: 0,
    radio: 0,
    district: {
      id: 0,
      name: '',
      province: {
        id: 0,
        name: '',
        region: {
          id: 0,
          name: ''
        } as Region
      } as Province
    } as District
  } as AreaInput
} as Sale
