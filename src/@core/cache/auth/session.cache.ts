import { User, UserKind } from '@core/modules/user'
import { Person } from '@core/modules/person'
import { Session } from '@core/modules/auth'

export default {
  user: {
    username: '',
    userKind: {
      id: 0,
      name: ''
    } as UserKind,
    person: {
      id: 0,
      documentNumber: '',
      name: '',
      photo: '',
      lastnameOne: '',
      lastnameTwo: ''
    } as Person,
    verified: false,
    status: false,
    lastSesion: ''
  } as User,
  isLogged: false
} as Session
