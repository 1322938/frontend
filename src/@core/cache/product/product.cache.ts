import { Product } from '@/@core/modules/product/interfaces/product.interface'
import { Category, Kind } from '@/@core/modules/product'

export default {
  id: 0,
  name: '',
  description: '',
  category: {
    id: 0,
    name: ''
  } as Category,
  kind: {
    id: 0,
    name: ''
  } as Kind
} as Product
