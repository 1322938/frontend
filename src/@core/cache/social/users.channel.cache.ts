import { ChatPersonSocial } from '@/@core/modules/social'

export default {
  channel: '',
  person: 0,
  documentNumber: '',
  photo: '',
  fullname: '',
  mobile: '',
  email: '',
  username: '',
  status: false
}as ChatPersonSocial
