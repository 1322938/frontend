import { Message } from '@/@core/modules/social'

export default {
  id: 0,
  created: '',
  channel: '',
  from: '',
  to: '',
  message: ''
} as Message
