import { PersonSocial } from '@/@core/modules/social'

export default {
  person: 0,
  documentNumber: '',
  photo: '',
  fullname: '',
  mobile: '',
  email: '',
  username: '',
  status: false
}as PersonSocial
